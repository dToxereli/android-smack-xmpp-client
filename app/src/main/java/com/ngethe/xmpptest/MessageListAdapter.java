package com.ngethe.xmpptest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ngethe.xmpptest.database.models.Chat;
import com.ngethe.xmpptest.database.models.ChatMessage;
import com.ngethe.xmpptest.database.Database;

import java.util.List;

public class MessageListAdapter extends BaseAdapter {
    private List<ChatMessage> chatMessages;
    private Context context;
    private Chat chat;
    private Database database;

    public MessageListAdapter(Context context, Chat chat) {
        this.context = context;
        this.chat = chat;
        this.database = new Database(context);
        this.chatMessages = database.getChatMessagesTable().getChatMessages(chat);
        chat.setUnreadMessagesCount(0);
        database.getChatsTable().update(chat);
    }

    public void addMessage(ChatMessage chatMessage) {
        chatMessages.add(chatMessage);
        database.getChatMessagesTable().insert(chatMessage);
        notifyDataSetChanged();
    }

    public void update() {
        this.chatMessages = database.getChatMessagesTable().getChatMessages(chat);
        database.getChatsTable().markRead(chat);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return chatMessages.size();
    }

    @Override
    public Object getItem(int i) {
        return chatMessages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ChatMessage chatMessage = (ChatMessage)getItem(i);
//        if (view != null) {
//            return view;
//        }
        if (chatMessage.getType() == ChatMessage.RECEIVED) {
            return getReceivedMessageView(chatMessage, view, viewGroup);
        } else {
            return getSentMessageView(chatMessage, view, viewGroup);
        }
    }

    public View getSentMessageView(ChatMessage chatMessage, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.bubble_sent_message, viewGroup, false);
        TextView txt_message = view.findViewById(R.id.txt_message);
        txt_message.setText(chatMessage.getMessage());
        return view;
    }

    public View getReceivedMessageView(ChatMessage chatMessage, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.bubble_received_message, viewGroup, false);
        TextView txt_message = view.findViewById(R.id.txt_message);
        txt_message.setText(chatMessage.getMessage());

        TextView name = view.findViewById(R.id.name);
        name.setText(chatMessage.getBuddy());

        return view;
    }
}
