package com.ngethe.xmpptest;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import android.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ngethe.xmpptest.database.models.Chat;
import com.ngethe.xmpptest.xmpp.XMPPService;
import com.ngethe.xmpptest.xmpp.service.XMPPBaseService;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    private ChatListAdapter chatListAdapter;
    private BroadcastReceiver xmppReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chatListAdapter = new ChatListAdapter(this);
        ListView lv_chats = findViewById(R.id.lv_chats);
        lv_chats.setAdapter(chatListAdapter);

        FloatingActionButton add_user = findViewById(R.id.add_user);
        add_user.setOnClickListener(this);
        FloatingActionButton add_group = findViewById(R.id.add_group);
        add_group.setOnClickListener(this);

        registerBroadcastListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBroadcastListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        chatListAdapter.updateData();
        registerBroadcastListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterBroadcastListener();
    }

    private void registerBroadcastListener() {
        if (xmppReceiver != null) {
            return;
        }

        xmppReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                chatListAdapter.updateData();
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(XMPPBaseService.EVENT_INCOMING_GROUP_MESSAGE);
        filter.addAction(XMPPBaseService.EVENT_INCOMING_P2P_MESSAGE);
        filter.addAction(XMPPBaseService.EVENT_NEW_GROUP);

        registerReceiver(xmppReceiver, filter);
    }

    private void unregisterBroadcastListener() {
        if (xmppReceiver == null) {
            return;
        }

        unregisterReceiver(xmppReceiver);
        xmppReceiver = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_user) {
            addUser();
        } else if (v.getId() == R.id.add_group) {
            addGroup();
        }
    }

    public void addUser() {
        AlertDialog dialog = TextInputDialog.getInstance(this, "Add user", "Username (No spaces allowed)", new TextInputDialog.TextInputDialogListener() {
            @Override
            public void onConfirm(String input) {
                addChatToDatabase(input, input + "@" + Config.HOSTNAME);
            }

            @Override
            public void onCancel() { }
        });
        dialog.show();
    }

    public void addGroup() {
        AlertDialog dialog = TextInputDialog.getInstance(this, "Add group", "Group name (No spaces allowed)", new TextInputDialog.TextInputDialogListener() {
            @Override
            public void onConfirm(String input) {
                addChatToDatabase(input, input + "@" + Config.MUC_HOSTNAME);
                Intent intent = new Intent(XMPPBaseService.ACTION_ADD_GROUP);
                intent.putExtra(XMPPBaseService.GROUP_JID_EXTRA, input + "@" + Config.MUC_HOSTNAME);
                XMPPService.enqueueWork(MainActivity.this, intent);
            }

            @Override
            public void onCancel() {

            }
        });
        dialog.show();
    }

    public void addChatToDatabase(String senderName, String senderFullAddress) {
        Chat chatRecord = new Chat();
        chatRecord.setSenderFullAddress(senderFullAddress);
        chatRecord.setSenderName(senderName);
        chatRecord.setModifiedAt(new Date());
        chatListAdapter.addChat(chatRecord);
    }
}
