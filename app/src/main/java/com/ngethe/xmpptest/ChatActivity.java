package com.ngethe.xmpptest;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.ngethe.xmpptest.database.models.Chat;
import com.ngethe.xmpptest.database.models.ChatMessage;
import com.ngethe.xmpptest.xmpp.XMPPService;
import com.ngethe.xmpptest.xmpp.service.XMPPBaseService;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String CHAT = "com.ngethe.xmpptest.extra.chat";

    private EditText txt_message;
    private MessageListAdapter messageListAdapter;
    private BroadcastReceiver xmppReceiver;

    Chat chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        txt_message = findViewById(R.id.txt_message);
        ImageButton btn_send_message = findViewById(R.id.btn_send_message);
        btn_send_message.setOnClickListener(this);

        chat = (Chat) getIntent().getSerializableExtra(CHAT);
        messageListAdapter = new MessageListAdapter(ChatActivity.this, chat);
        ListView messages_view = findViewById(R.id.messages_view);
        messages_view.setAdapter(messageListAdapter);

        ImageButton add_user = findViewById(R.id.add_user);
        if (!chat.getSenderFullAddress().contains(Config.MUC_HOSTNAME)) {
            add_user.setVisibility(View.GONE);
        }
        add_user.setOnClickListener(this);


        registerBroadcastListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        messageListAdapter.update();
        registerBroadcastListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterBroadcastListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBroadcastListener();
    }

    private void registerBroadcastListener() {
        if (xmppReceiver != null) {
            return;
        }

        xmppReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                messageListAdapter.update();
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(XMPPBaseService.EVENT_INCOMING_GROUP_MESSAGE);
        filter.addAction(XMPPBaseService.EVENT_INCOMING_P2P_MESSAGE);
        filter.addAction(XMPPBaseService.EVENT_NEW_GROUP);

        registerReceiver(xmppReceiver, filter);
    }

    private void unregisterBroadcastListener() {
        if (xmppReceiver == null) {
            return;
        }

        unregisterReceiver(xmppReceiver);
        xmppReceiver = null;
    }

    private void sendInvitation(String roomid, String userToInvite) {
        Intent intent = new Intent(XMPPBaseService.ACTION_SEND_GROUP_INVITE);
        intent.putExtra(XMPPBaseService.INVITE_ROOM_JID, roomid);
        intent.putExtra(XMPPBaseService.INVITE_TO_JID, userToInvite);
        XMPPService.enqueueWork(this, intent);
    }

    private void sendMessage(String sender, String message) {
        ChatMessage sentChatMessage = new ChatMessage(message, "You", ChatMessage.SENT);
        sentChatMessage.setChatId(chat.get_id());
        messageListAdapter.addMessage(sentChatMessage);

        Intent intent = new Intent(XMPPBaseService.ACTION_SEND_TEXT);
        intent.setAction(XMPPBaseService.ACTION_SEND_TEXT);
        intent.putExtra(XMPPBaseService.OUTGOING_TO_JID, sender);
        intent.putExtra(XMPPBaseService.OUTGOING_MESSAGE_BODY, message);
        XMPPService.enqueueWork(this, intent);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_send_message) {
            String message = txt_message.getText().toString();
            sendMessage(chat.getSenderFullAddress(), message);
            txt_message.setText("");
        } else if (v.getId() == R.id.add_user) {
            AlertDialog dialog = TextInputDialog.getInstance(this, "Invite user", "Username (No spaces allowed)", new TextInputDialog.TextInputDialogListener() {
                @Override
                public void onConfirm(String input) {
                    sendInvitation(chat.getSenderFullAddress(), input + "@" + Config.HOSTNAME);
                }

                @Override
                public void onCancel() {

                }
            });
            dialog.show();
        }
    }
}
