package com.ngethe.xmpptest;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.ngethe.xmpptest.xmpp.XMPPService;
import com.ngethe.xmpptest.xmpp.service.XMPPBaseService;

public class XMPPTestApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Intent intent = new Intent(XMPPBaseService.ACTION_CONNECT);
        XMPPService.enqueueWork(this, intent);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        Intent intent = new Intent(XMPPBaseService.ACTION_DISCONNECT);
        XMPPService.enqueueWork(this, intent);
    }
}
