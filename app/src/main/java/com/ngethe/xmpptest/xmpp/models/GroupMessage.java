package com.ngethe.xmpptest.xmpp.models;

import java.io.Serializable;

public class GroupMessage implements Serializable {
    private String groupName;
    private String groupJid;
    private String senderNickname;
    private String to;
    private String body;

    public GroupMessage(String groupName, String groupJid, String senderNickname, String to, String body) {
        this.groupName = groupName;
        this.groupJid = groupJid;
        this.senderNickname = senderNickname;
        this.to = to;
        this.body = body;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupJid() {
        return groupJid;
    }

    public String getSenderNickname() {
        return senderNickname;
    }

    public String getTo() {
        return to;
    }

    public String getBody() {
        return body;
    }
}
