package com.ngethe.xmpptest.xmpp.models;

import java.io.Serializable;

public class P2PMessage implements Serializable {
    private String fromUsername;
    private String fromJid;
    private String toUsername;
    private String toJid;
    private String body;

    public P2PMessage(String fromUsername, String fromJid, String toUsername, String toJid, String body) {
        this.fromUsername = fromUsername;
        this.fromJid = fromJid;
        this.toUsername = toUsername;
        this.toJid = toJid;
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public String getToJid() {
        return toJid;
    }

    public String getToUsername() {
        return toUsername;
    }

    public String getFromJid() {
        return fromJid;
    }

    public String getFromUsername() {
        return fromUsername;
    }
}
