package com.ngethe.xmpptest.xmpp.mucsub;

import android.util.Log;

import androidx.annotation.NonNull;

import org.jivesoftware.smack.packet.IQ;
import org.jxmpp.jid.Jid;

public class MucSubSubscribeIQ extends IQ {
    private static final String CHILD_ELEMENT_NAME = "subscribe";
    private static final String MUCSUB_NAMESPACE = "urn:xmpp:mucsub:0";

    private String nickname;
    private String userJid;
    private String password;

    public MucSubSubscribeIQ(@NonNull Jid currentUserJid, @NonNull Jid groupJid, @NonNull String nickname, String userJid, String password) {
        super(CHILD_ELEMENT_NAME, MUCSUB_NAMESPACE);
        this.nickname = nickname;
        this.userJid = userJid;
        this.password = password;
        setTo(groupJid);
        setFrom(currentUserJid);
        setType(Type.set);
    }

    public MucSubSubscribeIQ(@NonNull Jid currentUserJid, @NonNull Jid groupJid, @NonNull String nickname, String password) {
        this(currentUserJid, groupJid, nickname, null, password);
    }

    public MucSubSubscribeIQ(@NonNull Jid currentUserJid, @NonNull Jid groupJid, @NonNull String nickname) {
        this(currentUserJid, groupJid, nickname, null, null);
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.attribute("nick", nickname);
        if (userJid != null) xml.attribute("jid", userJid);
        if (password != null) xml.attribute("password", password);
        xml.rightAngleBracket();

        xml.halfOpenElement( "event ");
        xml.attribute("node", "urn:xmpp:mucsub:nodes:messages");
        xml.closeEmptyElement();

        xml.halfOpenElement( "event ");
        xml.attribute("node", "urn:xmpp:mucsub:nodes:affiliations");
        xml.closeEmptyElement();

        xml.halfOpenElement( "event ");
        xml.attribute("node", "urn:xmpp:mucsub:nodes:subject");
        xml.closeEmptyElement();

        xml.halfOpenElement( "event ");
        xml.attribute("node", "urn:xmpp:mucsub:nodes:config");
        xml.closeEmptyElement();

        xml.halfOpenElement( "event ");
        xml.attribute("node", "urn:xmpp:mucsub:nodes:presence");
        xml.closeEmptyElement();

        xml.halfOpenElement( "event ");
        xml.attribute("node", "urn:xmpp:mucsub:nodes:subscribers");
        xml.closeEmptyElement();

        return xml;
    }
}
