package com.ngethe.xmpptest.xmpp.muc;

import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.bookmarks.BookmarkManager;
import org.jivesoftware.smackx.bookmarks.BookmarkedConference;
import org.jivesoftware.smackx.mam.MamManager;
import org.jivesoftware.smackx.muc.Affiliate;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MucEnterConfiguration;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatException;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.muc.packet.MUCUser;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.parts.Resourcepart;
import org.jxmpp.stringprep.XmppStringprepException;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MucManager {
    private static Map<XMPPConnection, MucManager> xmppConnectionMucManagerMap = new HashMap<>();

    private WeakReference<XMPPConnection> connectionWeakReference;
    private BookmarkManager bookmarkManager;
    private MultiUserChatManager multiUserChatManager;
    private List<MucListener> listeners = new ArrayList<>();

    public static MucManager getInstanceFor(XMPPConnection connection) {
        MucManager mucManager = xmppConnectionMucManagerMap.get(connection);
        if (mucManager == null) {
            mucManager = new MucManager(connection);
            xmppConnectionMucManagerMap.put(connection, mucManager);
        }
        return mucManager;
    }

    private MucManager(XMPPConnection connection) {
        multiUserChatManager = MultiUserChatManager.getInstanceFor(connection);
        connectionWeakReference = new WeakReference<>(connection);
        bookmarkManager = BookmarkManager.getBookmarkManager(connection);

        multiUserChatManager.addInvitationListener(invitationListener);
        multiUserChatManager.setAutoJoinOnReconnect(true);
    }

    public List<BookmarkedConference> getBookmarkedRooms() throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        return bookmarkManager.getBookmarkedConferences();
    }

    public void joinBookmarkedGroups() throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException, XmppStringprepException, MultiUserChatException.NotAMucServiceException {
        for (BookmarkedConference conference : bookmarkManager.getBookmarkedConferences()) {
            MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(conference.getJid());
            joinMuc(mucChat, conference.getNickname(), conference.getPassword(), null);
        }
    }

    public void addMucToBookmarks(EntityBareJid mucJid, Resourcepart nickResource, String password) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        bookmarkManager.addBookmarkedConference(mucJid.toString(), mucJid, true, nickResource, password);
    }

    public void addMuc(EntityBareJid mucJid, String nick, String password, Date historySince) throws XmppStringprepException, XMPPException.XMPPErrorException, MultiUserChatException.NotAMucServiceException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        Resourcepart nickResource = Resourcepart.from(nick);
        joinMuc(mucChat, nickResource, password, historySince);
        addMucToBookmarks(mucJid, nickResource, null);
    }

    public void joinMuc(EntityBareJid mucJid, String nick, String password, Date historySince) throws InterruptedException, SmackException.NoResponseException, SmackException.NotConnectedException, XMPPException.XMPPErrorException, XmppStringprepException, MultiUserChatException.NotAMucServiceException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        Resourcepart nickResource = Resourcepart.from(nick);
        joinMuc(mucChat, nickResource, password, historySince);
    }

    private void joinMuc(final MultiUserChat mucChat, Resourcepart nickResource, String password, Date historySince) throws XmppStringprepException, XMPPException.XMPPErrorException, MultiUserChatException.NotAMucServiceException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        if (mucChat.isJoined()) return;

//        mucChat.addMessageListener(messageListener);

        mucChat.addMessageListener(new MessageListener() {
            @Override
            public void processMessage(Message message) {
                try {
                    Resourcepart currentUser = mucChat.getNickname();
                    Resourcepart otheruser = message.getFrom().getResourceOrNull();

                    if (currentUser == otheruser) {
                        for (MucListener listener : listeners) {
                            listener.outgoingMucMessage(message);
                        }
                    } else {
                        for (MucListener listener : listeners) {
                            listener.incomingMucMessage(message);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (historySince != null) {
            MucEnterConfiguration mucEnterConfiguration = mucChat.getEnterConfigurationBuilder(nickResource).requestHistorySince(historySince).withPassword(password).build();
            mucChat.join(mucEnterConfiguration);
        } else {
            mucChat.join(nickResource, password);
        }
    }


    public void leaveMuc(EntityBareJid mucJid) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        mucChat.leave();
        bookmarkManager.removeBookmarkedConference(mucJid);
    }

    public void invite(EntityBareJid mucJid, EntityBareJid userJid, String reason) throws SmackException.NotConnectedException, InterruptedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        mucChat.grantMembership(userJid);
        mucChat.invite(userJid, reason);
    }

    public void kick(EntityBareJid mucJid, EntityBareJid userJid, String reason) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        mucChat.revokeMembership(mucJid);
    }

    public List<Affiliate> getMembers(EntityBareJid mucJid) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        return mucChat.getMembers();
    }

    public List<Message> loadMessages(EntityBareJid mucJid, int max) throws XMPPException.XMPPErrorException, InterruptedException, SmackException.NotConnectedException, SmackException.NotLoggedInException, SmackException.NoResponseException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        MamManager mamManager = MamManager.getInstanceFor(mucChat);
        mamManager.enableMamForAllMessages();
        MamManager.MamQuery mamQuery = mamManager.queryMostRecentPage(mucJid, max);
        return mamQuery.getMessages();
    }

    public void sendMessage(EntityBareJid mucJid, String body) throws SmackException.NotConnectedException, InterruptedException {
        MultiUserChat mucChat = multiUserChatManager.getMultiUserChat(mucJid);
        Message message = mucChat.createMessage();
        message.setBody(body);
        mucChat.sendMessage(message);
    }

    public void addMucListener(MucListener mucListener) {
        listeners.add(mucListener);
    }

    public void removeMucListener(MucListener mucListener) {
        listeners.remove(mucListener);
    }

    private XMPPConnection connection() {
        return connectionWeakReference.get();
    }

    /**
     * Get the XMPPConnection of this Manager if it's authenticated, i.e. logged in. Otherwise throw a {@link SmackException.NotLoggedInException}.
     *
     * @return the XMPPConnection of this Manager.
     * @throws SmackException.NotLoggedInException if the connection is not authenticated.
     */
    private XMPPConnection getAuthenticatedConnectionOrThrow() throws SmackException.NotLoggedInException {
        XMPPConnection connection = connection();
        if (!connection.isAuthenticated()) {
            throw new SmackException.NotLoggedInException();
        }
        return connection;
    }

    private MessageListener messageListener = new MessageListener() {
        @Override
        public void processMessage(org.jivesoftware.smack.packet.Message message) {
            try {
                XMPPConnection connection = getAuthenticatedConnectionOrThrow();
                String authenticatedUser = connection.getUser().getLocalpart().toString();
                String sendingUser = connection.getUser().getLocalpart().toString();

                if (connection.getUser().getLocalpart().toString() == message.getFrom().getResourceOrThrow().toString()) {
                    for (MucListener listener : listeners) {
                        listener.outgoingMucMessage(message);
                    }
                } else {
                    for (MucListener listener : listeners) {
                        listener.incomingMucMessage(message);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private InvitationListener invitationListener = new InvitationListener() {
        @Override
        public void invitationReceived(XMPPConnection conn, MultiUserChat room, EntityJid inviter, String reason, String password, Message message, MUCUser.Invite invitation) {
            for (MucListener listener : listeners) {
                listener.onMucInvitationReceived(conn, room, inviter, reason, password, message, invitation);
            }
        }
    };
}
