package com.ngethe.xmpptest.xmpp.connection;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.ping.android.ServerPingWithAlarmManager;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class ConnectionManager {
    private static ConnectionManager instance;
    private final Map<String, XMPPTCPConnection> usernameXMPPTCPConnectionMap = new HashMap<>();

    public static ConnectionManager getInstance() {
        if (instance == null) {
            instance = new ConnectionManager();
        }
        return instance;
    }

    public void connect(String username, String hostname, String password, String resource, ConnectionListener listener) throws IOException, InterruptedException, XMPPException, SmackException {
        XMPPTCPConnection connection = getInstanceFor(username);
        if (connection != null) {
            if (connection.isAuthenticated()) {
                return;
            } else {
                connection.disconnect();
            }
        }

        InetAddress address = InetAddress.getByName(hostname);
        HostnameVerifier verifier = new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return false;
            }
        };
        XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                .setHost(username)
                .setUsernameAndPassword(username, password)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible)
                .allowEmptyOrNullUsernames()
                .setXmppDomain(hostname)
                .setHostnameVerifier(verifier)
                .setHostAddress(address)
                .setResource(resource)
                .setSendPresence(true)
                .setCompressionEnabled(true)
                .setPort(5222)
                .build();
        connection = new XMPPTCPConnection(config);

        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(connection);
        ReconnectionManager.setEnabledPerDefault(true);
        reconnectionManager.enableAutomaticReconnection();

        ServerPingWithAlarmManager.getInstanceFor(connection).setEnabled(true);
        PingManager pingManager = PingManager.getInstanceFor(connection);
        pingManager.setPingInterval(30);

        connection.setUseStreamManagement(true);
        connection.setUseStreamManagementResumption(false);

        if (listener != null) {
            connection.addConnectionListener(listener);
        }

        connection.connect();
        connection.login();

        if (connection.isAuthenticated()) {
            usernameXMPPTCPConnectionMap.put(username, connection);
        }
    }

    public void disconnect(String username) {
        XMPPTCPConnection connection = getInstanceFor(username);
        if (connection != null) {
            connection.disconnect();
        }
        usernameXMPPTCPConnectionMap.remove(username);
    }

    public boolean isConnected(String username) {
        XMPPTCPConnection connection = getInstanceFor(username);
        return connection != null && connection.isAuthenticated();
    }

    public synchronized XMPPTCPConnection getInstanceFor(String username) {
        synchronized (usernameXMPPTCPConnectionMap) {
            return usernameXMPPTCPConnectionMap.get(username);
        }
    }
}
