package com.ngethe.xmpptest.xmpp.mucsub;

import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.Jid;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class MucSubManager {
    private static Map<XMPPConnection, MucSubManager> managerMap = new HashMap<>();

    private WeakReference<XMPPConnection> connectionWeakReference;

    private MucSubManager(XMPPConnection connection) {
        connectionWeakReference = new WeakReference<>(connection);
    }

    public static MucSubManager getInstanceFor(XMPPConnection connection) {
        MucSubManager mucSubManager = managerMap.get(connection);
        if (mucSubManager == null) {
            mucSubManager = new MucSubManager(connection);
            managerMap.put(connection, mucSubManager);
        }
        return mucSubManager;
    }

    private XMPPConnection connection() {
        return connectionWeakReference.get();
    }

    /**
     * Get the XMPPConnection of this Manager if it's authenticated, i.e. logged in. Otherwise throw a {@link SmackException.NotLoggedInException}.
     *
     * @return the XMPPConnection of this Manager.
     * @throws SmackException.NotLoggedInException if the connection is not authenticated.
     */
    private XMPPConnection getAuthenticatedConnectionOrThrow() throws SmackException.NotLoggedInException {
        XMPPConnection connection = connection();
        if (!connection.isAuthenticated()) {
            throw new SmackException.NotLoggedInException();
        }
        return connection;
    }

    public IQ subscribe(Jid mucJid, String nickname, String password) throws SmackException.NotLoggedInException, XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        XMPPConnection authenticatedConnection = getAuthenticatedConnectionOrThrow();
        MucSubSubscribeIQ iq = new MucSubSubscribeIQ(authenticatedConnection.getUser().asEntityBareJid(), mucJid, nickname, password);
        return authenticatedConnection.sendIqRequestAndWaitForResponse(iq);
    }

    public IQ unsubscribe(Jid mucJid) throws SmackException.NotLoggedInException, XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        XMPPConnection authenticatedConnection = getAuthenticatedConnectionOrThrow();
        MucSubUnsubscribeIQ iq = new MucSubUnsubscribeIQ(authenticatedConnection.getUser().asEntityBareJid(), mucJid);
        return authenticatedConnection.sendIqRequestAndWaitForResponse(iq);
    }

    public IQ addSubscription(Jid mucJid, String nickname, String password, String userJid) throws SmackException.NotLoggedInException, XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        XMPPConnection authenticatedConnection = getAuthenticatedConnectionOrThrow();
        MucSubSubscribeIQ iq = new MucSubSubscribeIQ(authenticatedConnection.getUser().asEntityBareJid(), mucJid, nickname, userJid, password);
        return authenticatedConnection.sendIqRequestAndWaitForResponse(iq);
    }

    public IQ removeSubscription(Jid mucJid, String userJid) throws SmackException.NotLoggedInException, XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        XMPPConnection authenticatedConnection = getAuthenticatedConnectionOrThrow();
        MucSubUnsubscribeIQ iq = new MucSubUnsubscribeIQ(authenticatedConnection.getUser().asEntityBareJid(), mucJid, userJid);
        return authenticatedConnection.sendIqRequestAndWaitForResponse(iq);
    }

}
