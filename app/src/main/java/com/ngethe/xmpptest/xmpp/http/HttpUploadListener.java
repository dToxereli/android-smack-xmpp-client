package com.ngethe.xmpptest.xmpp.http;

import java.net.URL;

public interface HttpUploadListener {
    void onUploadDone(URL url);
    void onUploadProgress(double progress);
    void onUploadError(Throwable t);
}
