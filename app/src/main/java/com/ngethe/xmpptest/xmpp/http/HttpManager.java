package com.ngethe.xmpptest.xmpp.http;

import android.os.AsyncTask;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smackx.httpfileupload.HttpFileUploadManager;
import org.jivesoftware.smackx.httpfileupload.UploadProgressListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HttpManager {
    private static Map<XMPPConnection, HttpManager> connectionHttpManagerMap = new HashMap<>();
    private HttpFileUploadManager httpFileUploadManager;


    public static HttpManager getInstanceFor(XMPPConnection connection) {
        HttpManager httpManager = connectionHttpManagerMap.get(connection);
        if (httpManager == null) {
            httpManager = new HttpManager(connection);
            connectionHttpManagerMap.put(connection, httpManager);
        }
        return httpManager;
    }

    private HttpManager(XMPPConnection connection) {
        httpFileUploadManager = HttpFileUploadManager.getInstanceFor(connection);
    }

    public void uploadFile(final String filepath, final HttpUploadListener listener) {
        UploadAsyncTask uploadAsyncTask = new UploadAsyncTask(httpFileUploadManager, listener);
        uploadAsyncTask.execute(filepath);
    }

    public void downloadFile(final String url, final String filename, final String filepath, final HttpDownloadListener listener) {
        DownloadAsyncTask downloadAsyncTask = new DownloadAsyncTask(listener);
        downloadAsyncTask.execute(url, filename, filepath);
    }

    private static class UploadAsyncTask extends AsyncTask<String, Double, URL> {
        HttpUploadListener httpUploadListener;
        HttpFileUploadManager httpFileUploadManager;

        UploadAsyncTask(HttpFileUploadManager httpFileUploadManager, HttpUploadListener httpUploadListener) {
            this.httpFileUploadManager = httpFileUploadManager;
            this.httpUploadListener = httpUploadListener;
        }

        @Override
        protected URL doInBackground(String... strings) {
            String fileFullPath = strings[0];
            try {
                return httpFileUploadManager.uploadFile(new File(fileFullPath), new UploadProgressListener() {
                    @Override
                    public void onUploadProgress(long uploadedBytes, long totalBytes) {
                        publishProgress(((double)uploadedBytes/totalBytes));
                    }
                });
            } catch (Exception e) {
                httpUploadListener.onUploadError(e);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Double... values) {
            super.onProgressUpdate(values);
            Double percentComplete = values[0];
            if (percentComplete != null) {
                httpUploadListener.onUploadProgress(percentComplete);
            }
        }

        @Override
        protected void onPostExecute(URL url) {
            httpUploadListener.onUploadDone(url);
            super.onPostExecute(url);
        }
    };

    private static class DownloadAsyncTask extends AsyncTask<String , Double, String> {

        HttpDownloadListener httpDownloadListener;

        DownloadAsyncTask(HttpDownloadListener httpDownloadListener) {
            this.httpDownloadListener = httpDownloadListener;
        }

        @Override
        protected String doInBackground(String... strings) {
            String inputUrl = strings[0];
            String inputFileName = strings[1];
            String inputRootPathString = strings[2];
            String finalFilePath = inputRootPathString+"/"+ inputFileName;

            try {
                URL url = new URL(inputUrl);
                HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    httpDownloadListener.onDownloadError(new Throwable(connection.getResponseMessage()));
                    return null;
                }

                int fileLength = connection.getContentLength();
                InputStream input = connection.getInputStream();
                OutputStream output = new FileOutputStream(finalFilePath);

                byte data[] = new byte[4096];
                long total = 0;
                int count;

                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((double) (total / fileLength));
                    output.write(data, 0, count);
                }

                return finalFilePath;
            } catch (Exception e) {
                httpDownloadListener.onDownloadError(e);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Double... values) {
            super.onProgressUpdate(values);
            Double percentComplete = values[0];
            if (percentComplete != null) {
                httpDownloadListener.onDownloadProgressed(percentComplete);
            }
        }

        @Override
        protected void onPostExecute(String filepath) {
            super.onPostExecute(filepath);
            httpDownloadListener.onDownloadDone(filepath);
        }
    }
}
