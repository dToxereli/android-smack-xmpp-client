package com.ngethe.xmpptest.xmpp.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ngethe.xmpptest.xmpp.connection.ConnectionManager;
import com.ngethe.xmpptest.xmpp.http.HttpManager;
import com.ngethe.xmpptest.xmpp.http.HttpUploadListener;
import com.ngethe.xmpptest.xmpp.muc.MucListener;
import com.ngethe.xmpptest.xmpp.muc.MucManager;
import com.ngethe.xmpptest.xmpp.roster.RosterManager;
import com.ngethe.xmpptest.xmpp.single_chat.SingleChatListener;
import com.ngethe.xmpptest.xmpp.single_chat.SingleChatManager;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.bookmarks.BookmarkedConference;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public abstract class XMPPBaseService extends IntentService implements ConnectionListener, MucListener, SingleChatListener {
    public static final String TAG = XMPPBaseService.class.getSimpleName();

    // Intent Actions
    public static final String ACTION_CONNECT = "com.soc.nostra.co.ke.comms.chat.ACTION_CONNECT";
    public static final String ACTION_DISCONNECT = "com.soc.nostra.co.ke.comms.chat.ACTION_DISCONNECT";
    public static final String ACTION_SEND_TEXT = "com.soc.nostra.co.ke.comms.chat.ACTION_SEND_TEXT";
    public static final String ACTION_SEND_FILE= "com.soc.nostra.co.ke.comms.chat.ACTION_SEND_FIL";
    public static final String ACTION_SEND_GROUP_INVITE= "com.soc.nostra.co.ke.comms.chat.ACTION_SEND_GROUP_INVIT";
    public static final String ACTION_ADD_GROUP= "com.soc.nostra.co.ke.comms.chat.ACTION_ADD_GROUP";

    // Broadcast events emitted by this service
    public static final String EVENT_NEW_GROUP = "com.soc.nostra.co.ke.comms.chat.EVENT_NEW_GROUP";
    public static final String EVENT_INCOMING_P2P_MESSAGE = "com.soc.nostra.co.ke.comms.chat.EVENT_INCOMING_P2P_MESSAGE";
    public static final String EVENT_OUTGOING_P2P_MESSAGE = "com.soc.nostra.co.ke.comms.chat.EVENT_OUTGOING_P2P_MESSAGE";
    public static final String EVENT_INCOMING_GROUP_MESSAGE = "com.soc.nostra.co.ke.comms.chat.EVENT_INCOMING_GROUP_MESSAGE";
    public static final String EVENT_OUTGOING_GROUP_MESSAGE = "com.soc.nostra.co.ke.comms.chat.EVENT_OUTGOING_GROUP_MESSAGE";

    // Authentication credentials

    public static final String OUTGOING_MESSAGE_BODY = "com.soc.nostra.co.ke.comms.chat.OUTGOING_MESSAGE_BODY";
    public static final String OUTGOING_TO_JID = "com.soc.nostra.co.ke.comms.chat.OUTGOING_TO_JID";

    // P2P message parameters
    public static final String P2P_MESSAGE_BODY = "com.soc.nostra.co.ke.comms.chat.P2P_MESSAGE_BODY";
    public static final String P2P_FROM_NAME = "com.soc.nostra.co.ke.comms.chat.P2P_FROM_NAME";
    public static final String P2P_FROM_JID = "com.soc.nostra.co.ke.comms.chat.P2P_FROM_JID";

    // Group message parameters
    public static final String GROUP_MESSAGE_BODY_EXTRA = "com.soc.nostra.co.ke.comms.chat.GROUP_MESSAGE_BODY_EXTRA";
    public static final String GROUP_NAME_EXTRA = "com.soc.nostra.co.ke.comms.chat.GROUP_NAME_EXTRA";
    public static final String GROUP_FROM_NAME_EXTRA = "com.soc.nostra.co.ke.comms.chat.GROUP_FROM_NAME_EXTRA";
    public static final String GROUP_JID_EXTRA = "com.soc.nostra.co.ke.comms.chat.GROUP_JID_EXTRA";


    // XMPP Invitation parameters
    public static final String INVITE_ROOM_JID = "com.soc.nostra.co.ke.comms.chat.INVITE_ROOM_JID";
    public static final String INVITE_REASON = "com.soc.nostra.co.ke.comms.chat.INVITE_REASON";
    public static final String INVITE_TO_JID = "com.soc.nostra.co.ke.comms.chat.INVITE_TO_JID";


    protected final IBinder binder = new XMPPServiceBinder();

    public XMPPBaseService() {
        super("XMPPBaseService");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        switch (intent.getAction()) {
            case ACTION_CONNECT:
                handleConnectAction();
                break;
            case ACTION_DISCONNECT:
                handleDisconnectAction();
                break;
            case ACTION_SEND_TEXT:
                handleSendTextMessageAction(intent.getExtras());
                break;
            case ACTION_SEND_FILE:
                handleSendFileMessageAction(intent.getExtras());
                break;
            case ACTION_SEND_GROUP_INVITE:
                handleSendInviteAction(intent.getExtras());
                break;
            case ACTION_ADD_GROUP:
                handleAddMucAction(intent.getExtras());
                break;
            default:
                break;
        }
    }

    /**
     * This method gets a list of known user to apply roster visibility levels
     * @return A list of all known usernames
     */
    @NonNull
    protected abstract List<String> getKnownUsers();

    @NonNull
    protected abstract Date lastGroupJoinDate(EntityBareJid groupJid);

    @NonNull
    protected abstract String getUsername();

    @NonNull
    protected abstract String getPassword();

    @NonNull
    protected abstract String getHostname();

    @NonNull
    protected abstract String getResourceName();

    protected AbstractXMPPConnection getConnection() {
        String username = getUsername();
        ConnectionManager manager = ConnectionManager.getInstance();
        return manager.getInstanceFor(username);
    }

    public boolean isConnected() {
        ConnectionManager manager = ConnectionManager.getInstance();
        return manager.isConnected(getUsername());
    }

    protected void handleConnectAction() {
        String username = getUsername();
        String password = getPassword();
        String hostname = getHostname();
        String resource = getResourceName();

        ConnectionManager manager = ConnectionManager.getInstance();
        try {
            manager.connect(username, hostname, password, resource, this);
        } catch (Exception e) {
            Log.e(TAG, "Error occurred while connecting to XMPP domain " + hostname);
            e.printStackTrace();
        }
    }

    protected void handleDisconnectAction()
    {
        String username = getUsername();
        ConnectionManager manager = ConnectionManager.getInstance();
        manager.disconnect(username);
    }

    protected void handleAddMucAction(Bundle bundle) {
        String username = getUsername();
        String mucJid = bundle.getString(GROUP_JID_EXTRA);
        final XMPPConnection xmppConnection = getConnection();

        try {
            EntityBareJid jid = JidCreate.entityBareFrom(mucJid);
            MucManager mucManager = MucManager.getInstanceFor(xmppConnection);
            mucManager.addMuc(jid, username, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void handleSendTextMessageAction(Bundle bundle) {
        String buddy = bundle.getString(OUTGOING_TO_JID);
        String message = bundle.getString(OUTGOING_MESSAGE_BODY);
        final XMPPConnection xmppConnection = getConnection();

        if (buddy != null) {
            try {
                EntityBareJid buddyJid = JidCreate.entityBareFrom(buddy);
                if (buddy.contains("@conference.")) {
                    MucManager mucManager = MucManager.getInstanceFor(xmppConnection);
                    mucManager.sendMessage(buddyJid, message);
                } else {
                    SingleChatManager singleChatManager = SingleChatManager.getInstanceFor(xmppConnection);
                    singleChatManager.sendMessage(buddyJid, message);
                }
            } catch (SmackException.NotConnectedException | InterruptedException | XmppStringprepException e) {
                e.printStackTrace();
            }
        }
    }

    protected void handleSendFileMessageAction(Bundle bundle) {
        final String buddy = bundle.getString(OUTGOING_TO_JID);
        final String message = bundle.getString(OUTGOING_MESSAGE_BODY);
        final XMPPConnection xmppConnection = getConnection();

        if (buddy != null) {
            HttpManager httpManager = HttpManager.getInstanceFor(xmppConnection);
            httpManager.uploadFile(message, new HttpUploadListener() {
                @Override
                public void onUploadDone(URL url) {
                    try {
                        EntityBareJid buddyJid = JidCreate.entityBareFrom(buddy);
                        if (buddy.contains("@conference.")) {
                            MucManager mucManager = MucManager.getInstanceFor(xmppConnection);
                            mucManager.sendMessage(buddyJid, url.toString());
                        } else {
                            SingleChatManager singleChatManager = SingleChatManager.getInstanceFor(xmppConnection);
                            singleChatManager.sendMessage(buddyJid, url.toString());
                        }
                    } catch (SmackException.NotConnectedException | InterruptedException | XmppStringprepException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onUploadProgress(double progress) {

                }

                @Override
                public void onUploadError(Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    protected void handleSendInviteAction(Bundle bundle) {
        final XMPPConnection xmppConnection = getConnection();
        MucManager mucManager = MucManager.getInstanceFor(xmppConnection);
        String buddy = bundle.getString(INVITE_TO_JID);
        String muc = bundle.getString(INVITE_ROOM_JID);
        String reason = bundle.getString(INVITE_REASON, "You have been invited to join this group");

        try {
            EntityBareJid buddyJid = JidCreate.entityBareFrom(buddy);
            EntityBareJid mucJid = JidCreate.entityBareFrom(muc);
            mucManager.invite(mucJid, buddyJid, reason);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //* ConnectionListener implementation

    @Override
    public void connected(XMPPConnection connection) {
        Log.i(TAG, "Connected");
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        Log.i(TAG, "Authenticated");
        try {
            SingleChatManager singleChatManager = SingleChatManager.getInstanceFor(connection);
            singleChatManager.addSingleChatListener(this);

            MucManager mucManager = MucManager.getInstanceFor(connection);
            for (BookmarkedConference conference : mucManager.getBookmarkedRooms()) {
                String senderFullAddress = conference.getJid().asUnescapedString();
                Date lastViewDate = lastGroupJoinDate(conference.getJid());
                mucManager.joinMuc(conference.getJid(), conference.getNickname().toString(), conference.getPassword(), lastViewDate);
            }
            mucManager.addMucListener(this);

            connection.sendStanza(new Presence(Presence.Type.available));

            RosterManager rosterManager = RosterManager.getInstanceFor(connection);
            rosterManager.setKnownContacts(getKnownUsers());
            rosterManager.setAdvertisement(RosterManager.OnlineStatusAdvertisement.PUBLIC);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionClosed() {
        Log.i(TAG, "Disconnected");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        Log.i(TAG, "Disconnected on error");
        e.printStackTrace();
    }


    public class XMPPServiceBinder extends Binder {
        XMPPBaseService getService() {
            return XMPPBaseService.this;
        }
    }
}
