package com.ngethe.xmpptest.xmpp.http;

public interface HttpDownloadListener {
    void onDownloadDone(String filepath);
    void onDownloadProgressed(double progress);
    void onDownloadError(Throwable t);
}
