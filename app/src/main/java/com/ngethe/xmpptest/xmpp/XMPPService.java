package com.ngethe.xmpptest.xmpp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.ngethe.xmpptest.ChatActivity;
import com.ngethe.xmpptest.Config;
import com.ngethe.xmpptest.MainActivity;
import com.ngethe.xmpptest.R;
import com.ngethe.xmpptest.database.models.ChatMessage;
import com.ngethe.xmpptest.database.Database;
import com.ngethe.xmpptest.xmpp.muc.MucManager;
import com.ngethe.xmpptest.xmpp.service.XMPPBaseService;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.packet.MUCUser;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.parts.Resourcepart;

import java.util.Date;
import java.util.List;

public class XMPPService extends XMPPBaseService {
    private Database database;

    private static final int JOB_ID = 10000;
    final String CHANNEL_ID = "com.soc.nostra.co.ke.comms.chat.CHANNEL_RECEIVED_MESSAGE";

    public static void enqueueWork(Context context, Intent work) {
        work.setClass(context, XMPPService.class);
        context.startService(work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        database = new Database(this);
        Log.i(TAG, "Service started");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Service stopped");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    private com.ngethe.xmpptest.database.models.Chat getOrCreate(String senderName, String senderFullAddress) {
        com.ngethe.xmpptest.database.models.Chat chatRecord = database.getChatsTable().getBySenderFullAddress(senderFullAddress);
        if (chatRecord == null) {
            chatRecord = new com.ngethe.xmpptest.database.models.Chat();
            chatRecord.setSenderFullAddress(senderFullAddress);
            chatRecord.setSenderName(senderName);
            chatRecord.setModifiedAt(new Date());
            chatRecord = database.getChatsTable().insert(chatRecord);
        }

        return chatRecord;
    }

    private void saveMessage(com.ngethe.xmpptest.database.models.Chat chat, String message, String sender) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessage(message);
        chatMessage.setBuddy(sender);
        chatMessage.setType(ChatMessage.RECEIVED);
        chatMessage.setChatId(chat.get_id());
        chatMessage.setCreatedAt(new Date());

        database.getChatMessagesTable().insert(chatMessage);

        chat.addUnreadMessages(1);
        chat.setModifiedAt(new Date());
        database.getChatsTable().update(chat);
    }

    @NonNull
    @Override
    protected List<String> getKnownUsers() {
        return database.getChatsTable().contacts();
    }

    @NonNull
    @Override
    protected Date lastGroupJoinDate(EntityBareJid groupJid) {
        String roomName = groupJid.getLocalpartOrNull().toString();
        String senderFullAddress = groupJid.asUnescapedString();
        com.ngethe.xmpptest.database.models.Chat chatRecord = getOrCreate(roomName, senderFullAddress);
        return chatRecord.getModifiedAt();
    }

    @NonNull
    @Override
    protected String getUsername() {
        return Config.USERNAME;
    }

    @NonNull
    @Override
    protected String getPassword() {
        return Config.PASSWORD;
    }

    @NonNull
    @Override
    protected String getHostname() {
        return Config.HOSTNAME;
    }

    @NonNull
    @Override
    protected String getResourceName() {
        return Config.RESOURCE;
    }

    @Override
    public void incomingMucMessage(Message message) {
        String senderFullAddress = message.getFrom().asBareJid().asUnescapedString();
        String roomName = message.getFrom().getLocalpartOrNull().toString();
        String messageBody = message.getBody();
        Resourcepart senderResource = message.getFrom().getResourceOrNull();
        if (messageBody != null && senderResource != null) {
            String senderName = senderResource.toString();
            com.ngethe.xmpptest.database.models.Chat chatRecord = getOrCreate(roomName, senderFullAddress);
            saveMessage(chatRecord, messageBody, senderName);

            Intent intent = new Intent(EVENT_INCOMING_GROUP_MESSAGE);
            intent.putExtra(GROUP_FROM_NAME_EXTRA, senderName);
            intent.putExtra(GROUP_NAME_EXTRA, roomName);
            intent.putExtra(GROUP_JID_EXTRA, senderFullAddress);
            intent.putExtra(GROUP_MESSAGE_BODY_EXTRA, messageBody);
            sendBroadcast(intent);
            showNotification(chatRecord, roomName, senderName , messageBody);
        }


        Log.i(TAG, "Received a new group message: " + messageBody + " from " + roomName);

    }

    @Override
    public void outgoingMucMessage(Message message) {
        Log.i(TAG, "Group chat message sent");
    }

    @Override
    public void onMucInvitationReceived(XMPPConnection conn, MultiUserChat room, EntityJid inviter, String reason, String password, Message message, MUCUser.Invite invitation) {
        MucManager mucManager = MucManager.getInstanceFor(conn);
        try {
            mucManager.addMuc(room.getRoom(), Config.USERNAME, password, new Date());

            Intent intent = new Intent(EVENT_NEW_GROUP);

            String senderFullAddress = room.getRoom().asBareJid().asUnescapedString();
            String roomName = room.getRoom().getLocalpartOrNull().toString();
            intent.putExtra(GROUP_NAME_EXTRA, roomName);
            intent.putExtra(GROUP_JID_EXTRA, senderFullAddress);
            sendBroadcast(intent);

            Log.i(TAG, "Received a room invitation: " + reason);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void incomingP2PMessage(EntityBareJid from, Message message, Chat chat) {
        String senderFullAddress = from.toString();
        String senderName = from.getLocalpart().asUnescapedString();
        String messageBody = message.getBody();

        com.ngethe.xmpptest.database.models.Chat chatRecord = getOrCreate(senderName, senderFullAddress);
        saveMessage(chatRecord, messageBody, senderName);

        Intent intent = new Intent(EVENT_INCOMING_P2P_MESSAGE);
        intent.putExtra(P2P_FROM_NAME, senderName);
        intent.putExtra(P2P_FROM_JID, senderFullAddress);
        intent.putExtra(P2P_MESSAGE_BODY, messageBody);
        sendBroadcast(intent);

        showNotification(chatRecord, senderName, senderName, messageBody);

        Log.i(TAG, "Received a new p2p message: " + messageBody + " from " + senderName);

    }

    @Override
    public void outgoingP2PMessage(EntityBareJid to, Message message, Chat chat) {
        Log.i(TAG, "P2P chat message sent");
    }

    private void showNotification(com.ngethe.xmpptest.database.models.Chat chat, String title, String sender, String message) {
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }

        Intent mainActivityInten = new Intent(this, MainActivity.class);
        mainActivityInten.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        Intent intent = new Intent(this, ChatActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(ChatActivity.CHAT, chat);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 48945649, new Intent[] {mainActivityInten, intent}, PendingIntent.FLAG_CANCEL_CURRENT);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round);
        Notification mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_send)
                .setLargeIcon(icon)
                .setContentTitle(title) // title for notification
                .setContentText(sender+": "+ message) // message for notification
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .build();

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);
        }
        int MESSAGE_NOTIFICATION_ID = 876543210;
        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone ringtone = RingtoneManager.getRingtone(this, notification);
                ringtone.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
