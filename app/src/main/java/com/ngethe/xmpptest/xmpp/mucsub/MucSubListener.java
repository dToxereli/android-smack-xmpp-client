package com.ngethe.xmpptest.xmpp.mucsub;

import org.jivesoftware.smack.packet.Message;

public interface MucSubListener {
    void onMessage(Message message);
}
