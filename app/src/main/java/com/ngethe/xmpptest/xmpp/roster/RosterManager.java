package com.ngethe.xmpptest.xmpp.roster;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.roster.SubscribeListener;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.parts.Localpart;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RosterManager implements SubscribeListener {
    private static Map<XMPPConnection, RosterManager> connectionRosterManagerHashMap = new HashMap<>();
    private final Roster roster;
    private final VCardManager vCardManager;
    private List<String> knownContacts = new ArrayList<>();
    private OnlineStatusAdvertisement advertisement = OnlineStatusAdvertisement.PUBLIC;


    public static RosterManager getInstanceFor(XMPPConnection connection) {
        RosterManager rosterManager = connectionRosterManagerHashMap.get(connection);
        if (rosterManager == null) {
            rosterManager = new RosterManager(connection);
            connectionRosterManagerHashMap.put(connection, rosterManager);
        }
        return rosterManager;
    }

    private RosterManager(XMPPConnection connection) {

        roster = Roster.getInstanceFor(connection);
        roster.setSubscriptionMode(Roster.SubscriptionMode.manual);
        roster.addSubscribeListener(this);

        vCardManager = VCardManager.getInstanceFor(connection);
    }

    public void setKnownContacts(List<String> knownContacts) {
        this.knownContacts = knownContacts;
    }

    public void setAdvertisement(OnlineStatusAdvertisement advertisement) {
        this.advertisement = advertisement;
    }

    public VCard getUserVCard(EntityBareJid userJid) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        return vCardManager.loadVCard(userJid);
    }

    public void updateVCard(byte[] image) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException, InterruptedException, SmackException.NoResponseException {
        VCard vCard = vCardManager.loadVCard();
        vCard.setAvatar(image);
        vCardManager.saveVCard(vCard);
    }

    public Presence getUserPresence(BareJid userJid) {
        return roster.getPresence(userJid);
    }

    public void addRosterListener(RosterListener listener) {
        roster.addRosterListener(listener);
    }

    public void removeRosterListener(RosterListener listener) {
        roster.removeRosterListener(listener);
    }

    @Override
    public SubscribeAnswer processSubscribe(Jid from, Presence subscribeRequest) {
        if (advertisement == OnlineStatusAdvertisement.PUBLIC) {
            return SubscribeAnswer.ApproveAndAlsoRequestIfRequired;
        } else if (advertisement == OnlineStatusAdvertisement.CONTACTS_ONLY) {
            Localpart sender = from.getLocalpartOrNull();
            if (sender != null && knownContacts.contains(sender.toString())) {
                return SubscribeAnswer.ApproveAndAlsoRequestIfRequired;
            }
        }
        return SubscribeAnswer.Deny;
    }

    public enum  OnlineStatusAdvertisement {
        HIDDEN,
        CONTACTS_ONLY,
        PUBLIC
    }
}
