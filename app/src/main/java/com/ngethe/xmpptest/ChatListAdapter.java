package com.ngethe.xmpptest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ngethe.xmpptest.database.models.Chat;
import com.ngethe.xmpptest.database.models.ChatMessage;
import com.ngethe.xmpptest.database.Database;

import java.util.ArrayList;
import java.util.Map;

public class ChatListAdapter extends BaseAdapter {
    private ArrayList<Chat> chatList;
    private Context context;
    private Database database;
    private Map<Long, ChatMessage> lastMessages;

    public ChatListAdapter(Context context) {
        this.context = context;
        this.database = new Database(context);
        this.chatList = database.getChatsTable().all();
        this.lastMessages = database.getChatMessagesTable().lastMessages();
    }

    public void setContext(Context context) {
        this.context = context;
        if (context != null) {
            this.database = new Database(context);
            this.chatList = database.getChatsTable().all();
        } else {
            this.chatList = new ArrayList<>();
        }
        notifyDataSetChanged();
    }

    public void updateData() {
        if (database != null) {
            this.chatList = database.getChatsTable().all();
            this.lastMessages = database.getChatMessagesTable().lastMessages();
            notifyDataSetChanged();
        }
    }

    public void addChat(Chat chat) {
        database.getChatsTable().insert(chat);
        chatList.add(chat);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return chatList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return chatList.get(position).get_id();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.card_chat, parent, false);
        }
        final Chat chat = chatList.get(position);
        TextView tv_name = convertView.findViewById(R.id.tv_name);
        tv_name.setText(chat.getSenderName());
        TextView tv_date = convertView.findViewById(R.id.tv_date);
        tv_date.setText(chat.getTimeStamp());

        TextView tv_latest_message = convertView.findViewById(R.id.tv_latest_message);
        ChatMessage message = lastMessages.get(chat.get_id());
        if (message != null) {
            SpannableString spanString = new SpannableString(message.toString());
            if (chat.getUnreadMessagesCount() > 0) {
                spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
            }
            tv_latest_message.setText(spanString);
        }

        TextView tv_unread_messages = convertView.findViewById(R.id.tv_unread_messages);
        tv_unread_messages.setText(String.valueOf(chat.getUnreadMessagesCount()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra(ChatActivity.CHAT, chat);
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
