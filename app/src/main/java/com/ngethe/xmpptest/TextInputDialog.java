package com.ngethe.xmpptest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

public class TextInputDialog extends DialogFragment {
    public static AlertDialog getInstance(Context context, String title, String hint, final TextInputDialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);

        builder.setTitle(title);

        View dialog_text_input = inflater.inflate(R.layout.dialog_text_input, null);
        final EditText input = dialog_text_input.findViewById(R.id.input);
        input.setHint(hint);

        builder.setView(dialog_text_input)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) listener.onConfirm(input.getText().toString().trim());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) listener.onCancel();
                    }
                });
        return builder.create();
    }

    public interface TextInputDialogListener {
        void onConfirm(String input);
        void onCancel();
    }
}
