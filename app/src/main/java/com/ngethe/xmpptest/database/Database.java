package com.ngethe.xmpptest.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.ngethe.xmpptest.database.models.Chat;
import com.ngethe.xmpptest.database.models.ChatMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database extends SQLiteOpenHelper {
    public static final int VERSION = 1;

    private ChatsTable chatsTable;
    private ChatMessagesTable chatMessagesTable;
    private Context context;

    public Database(@Nullable Context context) {
        super(context, "xmpptest.db", null, VERSION);
        this.context = context;
        chatsTable = new ChatsTable();
        chatMessagesTable = new ChatMessagesTable();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        chatsTable.create(db);
        chatMessagesTable.create(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        chatsTable.reset(db);
        chatMessagesTable.reset(db);
    }

    public ChatsTable getChatsTable() {
        return chatsTable;
    }

    public ChatMessagesTable getChatMessagesTable() {
        return chatMessagesTable;
    }

    public class ChatsTable {
        private static final String TABLE_NAME = "chats";
        private static final String CHAT_ID = "_id";
        private static final String CHAT_SENDER_NAME = "sender_name";
        private static final String CHAT_SENDER_FULL_ADDRESS = "sender_full_address";
        private static final String CHAT_UNREAD_MESSAGES_COUNT = "unread_messages_count";
        private static final String CHAT_MODIFIED_AT = "modified_at";

        private static final int CHAT_ID_INDEX = 0;
        private static final int CHAT_SENDER_NAME_INDEX = 1;
        private static final int CHAT_SENDER_FULL_ADDRESS_INDEX = 2;
        private static final int CHAT_UNREAD_MESSAGES_COUNT_INDEX = 3;
        private static final int CHAT_MODIFIED_AT_INDEX = 4;


        void create(SQLiteDatabase database) {
            String query = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                    CHAT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    CHAT_SENDER_NAME + " TEXT," +
                    CHAT_SENDER_FULL_ADDRESS + " TEXT UNIQUE," +
                    CHAT_UNREAD_MESSAGES_COUNT + " INTEGER," +
                    CHAT_MODIFIED_AT + " TEXT)";
            database.execSQL(query);
        }

        void drop(SQLiteDatabase database) {
            String query = "DROP TABLE IF EXISTS " + TABLE_NAME;
            database.execSQL(query);
        }

        void reset(SQLiteDatabase database) {
            drop(database);
            create(database);
        }

        public Chat insert(Chat chat) {
            SQLiteDatabase database = getWritableDatabase();
            ContentValues values=new ContentValues();
            values.put(CHAT_SENDER_NAME, chat.getSenderName());
            values.put(CHAT_SENDER_FULL_ADDRESS, chat.getSenderFullAddress());
            values.put(CHAT_UNREAD_MESSAGES_COUNT, chat.getUnreadMessagesCount());
            values.put(CHAT_MODIFIED_AT, chat.getTimeStamp());
            long newId = database.insert(TABLE_NAME, null, values);
            chat.set_id(newId);
            database.close();
            return chat;
        }

        public void update(Chat chat) {
            SQLiteDatabase database = getWritableDatabase();
            ContentValues values=new ContentValues();
            values.put(CHAT_SENDER_NAME, chat.getSenderName());
            values.put(CHAT_SENDER_FULL_ADDRESS, chat.getSenderFullAddress());
            values.put(CHAT_UNREAD_MESSAGES_COUNT, chat.getUnreadMessagesCount());
            values.put(CHAT_MODIFIED_AT, chat.getTimeStamp());

            String whereClause = "_id = ?";
            String[] whereArgs = new String[]{ String.valueOf(chat.get_id()) };

            database.update(TABLE_NAME, values, whereClause, whereArgs);
            database.close();
        }

        public void markRead(Chat chat) {
            SQLiteDatabase database = getWritableDatabase();
            String whereClause = "_id = ?";
            String[] whereArgs = new String[]{ String.valueOf(chat.get_id()) };
            ContentValues values=new ContentValues();
            values.put(CHAT_UNREAD_MESSAGES_COUNT, 0);
            database.update(TABLE_NAME, values, whereClause, whereArgs);
            database.close();
        }

        public void delete(Chat chat) {
            SQLiteDatabase database = getWritableDatabase();
            String whereClause = "_id = ?";
            String[] whereArgs = new String[]{ String.valueOf(chat.get_id()) };
            database.delete(TABLE_NAME, whereClause, whereArgs);
            getChatMessagesTable().deleteChatMessages(chat);
            database.close();
        }

        public ArrayList<Chat> all() {
            SQLiteDatabase database = getReadableDatabase();
            ArrayList<Chat> chats = new ArrayList<>();
            String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY "+ CHAT_MODIFIED_AT +" DESC";
            Cursor cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    Chat chat = new Chat();
                    chat.set_id(cursor.getLong(CHAT_ID_INDEX));
                    chat.setSenderName(cursor.getString(CHAT_SENDER_NAME_INDEX));
                    chat.setSenderFullAddress(cursor.getString(CHAT_SENDER_FULL_ADDRESS_INDEX));
                    chat.setUnreadMessagesCount(cursor.getInt(CHAT_UNREAD_MESSAGES_COUNT_INDEX));
                    chat.setModifiedAt(cursor.getString(CHAT_MODIFIED_AT_INDEX));
                    chats.add(chat);
                } while (cursor.moveToNext());
            }
            cursor.close();
            database.close();
            return chats;
        }

        public ArrayList<String> contacts() {
            SQLiteDatabase database = getReadableDatabase();
            ArrayList<String> chats = new ArrayList<>();
            String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY "+ CHAT_MODIFIED_AT +" DESC";
            Cursor cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    chats.add(cursor.getString(CHAT_SENDER_NAME_INDEX));
                } while (cursor.moveToNext());
            }
            cursor.close();
            database.close();
            return chats;
        }

        public Chat getById(long _id) {
            Chat chat = null;
            SQLiteDatabase database = getReadableDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE "+CHAT_ID+"="+_id+" LIMIT 1";
            Cursor cursor = database.rawQuery(query, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    chat = new Chat();
                    chat.set_id(cursor.getLong(CHAT_ID_INDEX));
                    chat.setSenderName(cursor.getString(CHAT_SENDER_NAME_INDEX));
                    chat.setSenderFullAddress(cursor.getString(CHAT_SENDER_FULL_ADDRESS_INDEX));
                    chat.setUnreadMessagesCount(cursor.getInt(CHAT_UNREAD_MESSAGES_COUNT_INDEX));
                    chat.setModifiedAt(cursor.getString(CHAT_MODIFIED_AT_INDEX));
                }
                cursor.close();
            }
            database.close();
            return chat;
        }

        public Chat getBySenderFullAddress(String senderFullAddress) {
            Chat chat = null;
            SQLiteDatabase database = getReadableDatabase();
//            String query = "SELECT * FROM " + TABLE_NAME + " WHERE "+CHAT_SENDER_FULL_ADDRESS+"=\'"+senderFullAddress+"\' LIMIT 1";
//            Cursor cursor = database.rawQuery(query, null);

            String whereClause = CHAT_SENDER_FULL_ADDRESS + " = ?";
            String[] whereArgs = new String[]{ senderFullAddress };
            Cursor cursor = database.query(TABLE_NAME, null, whereClause, whereArgs, null, null, null, "1");

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    chat = new Chat();
                    chat.set_id(cursor.getLong(CHAT_ID_INDEX));
                    chat.setSenderName(cursor.getString(CHAT_SENDER_NAME_INDEX));
                    chat.setSenderFullAddress(cursor.getString(CHAT_SENDER_FULL_ADDRESS_INDEX));
                    chat.setUnreadMessagesCount(cursor.getInt(CHAT_UNREAD_MESSAGES_COUNT_INDEX));
                    chat.setModifiedAt(cursor.getString(CHAT_MODIFIED_AT_INDEX));
                }
                cursor.close();
            }
            database.close();
            return chat;
        }

        public Chat getOrCreate(String senderFullAddress) {
            Chat chat = getBySenderFullAddress(senderFullAddress);
            if (chat == null) {
                chat = insert(chat);
            }
            return chat;
        }
    }

    public class ChatMessagesTable {
        private static final String TABLE_NAME = "chat_messages";

        private static final String _ID = "_id";
        private static final String MESSAGE = "message";
        private static final String BUDDY = "buddy";
        private static final String TYPE = "type";
        private static final String CHAT_ID = "chat_id";
        private static final String CREATED_AT = "created_at";

        private static final int _ID_INDEX = 0;
        private static final int MESSAGE_INDEX = 1;
        private static final int BUDDY_INDEX = 2;
        private static final int TYPE_INDEX = 3;
        private static final int CHAT_ID_INDEX = 4;
        private static final int CREATED_AT_INDEX = 5;

        void create(SQLiteDatabase database) {
            String query = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    MESSAGE + " TEXT," +
                    BUDDY + " TEXT," +
                    TYPE + " INTEGER," +
                    CHAT_ID + " INTEGER," +
                    CREATED_AT + " TEXT)";
            database.execSQL(query);
        }

        void drop(SQLiteDatabase database) {
            String query = "DROP TABLE IF EXISTS " + TABLE_NAME;
            database.execSQL(query);
        }

        void reset(SQLiteDatabase database) {
            drop(database);
            create(database);
        }

        public ChatMessage insert(ChatMessage chatMessage) {
            SQLiteDatabase database = getWritableDatabase();
            ContentValues values=new ContentValues();
            values.put(MESSAGE, chatMessage.getMessage());
            values.put(BUDDY, chatMessage.getBuddy());
            values.put(CHAT_ID, chatMessage.getChatId());
            values.put(TYPE, chatMessage.getType());
            values.put(CREATED_AT, chatMessage.getTimestamp());
            long newId = database.insert(TABLE_NAME, null, values);
            chatMessage.set_id(newId);
            database.close();
            return chatMessage;
        }

        public void delete(ChatMessage chatMessage) {
            SQLiteDatabase database = getWritableDatabase();
            String whereClause = "_id = ?";
            String[] whereArgs = new String[]{ String.valueOf(chatMessage.get_id()) };
            database.delete(TABLE_NAME, whereClause, whereArgs);
            database.close();
        }

        public void deleteChatMessages(Chat chat) {
            SQLiteDatabase database = getWritableDatabase();

            database.close();
        }

        public List<ChatMessage> all() {
            SQLiteDatabase database = getReadableDatabase();
            List<ChatMessage> chatMessages = new ArrayList<>();
            Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, CREATED_AT);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        ChatMessage chatMessage = new ChatMessage();
                        chatMessage.set_id(cursor.getLong(_ID_INDEX));
                        chatMessage.setMessage(cursor.getString(MESSAGE_INDEX));
                        chatMessage.setBuddy(cursor.getString(BUDDY_INDEX));
                        chatMessage.setChatId(cursor.getLong(CHAT_ID_INDEX));
                        chatMessage.setType(cursor.getInt(TYPE_INDEX));
                        chatMessage.setCreatedAt(cursor.getString(CREATED_AT_INDEX));
                        chatMessages.add(chatMessage);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            database.close();
            return chatMessages;
        }

        public Map<Long, ChatMessage> lastMessages() {
            SQLiteDatabase database = getReadableDatabase();
            Map<Long, ChatMessage> chatMessages = new HashMap<>();
            Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY " + _ID, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        ChatMessage chatMessage = new ChatMessage();
                        chatMessage.set_id(cursor.getLong(_ID_INDEX));
                        chatMessage.setMessage(cursor.getString(MESSAGE_INDEX));
                        chatMessage.setBuddy(cursor.getString(BUDDY_INDEX));
                        chatMessage.setChatId(cursor.getLong(CHAT_ID_INDEX));
                        chatMessage.setType(cursor.getInt(TYPE_INDEX));
                        chatMessage.setCreatedAt(cursor.getString(CREATED_AT_INDEX));
                        chatMessages.put(chatMessage.getChatId(), chatMessage);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            database.close();
            return chatMessages;
        }

        public List<ChatMessage> getChatMessages(Chat chat) {
            SQLiteDatabase database = getReadableDatabase();
            List<ChatMessage> chatMessages = new ArrayList<>();
            String whereClause = "chat_id = ?";
            String[] whereArgs = new String[]{ String.valueOf(chat.get_id()) };
            Cursor cursor = database.query(TABLE_NAME, null, whereClause, whereArgs, null, null, CREATED_AT);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        ChatMessage chatMessage = new ChatMessage();
                        chatMessage.set_id(cursor.getLong(_ID_INDEX));
                        chatMessage.setMessage(cursor.getString(MESSAGE_INDEX));
                        chatMessage.setBuddy(cursor.getString(BUDDY_INDEX));
                        chatMessage.setChatId(cursor.getLong(CHAT_ID_INDEX));
                        chatMessage.setType(cursor.getInt(TYPE_INDEX));
                        chatMessage.setCreatedAt(cursor.getString(CREATED_AT_INDEX));
                        chatMessages.add(chatMessage);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            database.close();
            return chatMessages;
        }
    }
}
