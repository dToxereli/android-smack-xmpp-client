package com.ngethe.xmpptest.database.models;

import androidx.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ChatMessage {
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final int SENT = 1;
    public static final int RECEIVED = 2;

    private long _id;
    private String message;
    private String buddy;
    private long chatId;
    private int type;
    private Date createdAt = new Date();

    public ChatMessage() { }

    public ChatMessage(String message, String buddy, int type) {
        this.message = message;
        this.buddy = buddy;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBuddy() {
        return buddy;
    }

    public void setBuddy(String buddy) {
        this.buddy = buddy;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getTimestamp() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        return sdf.format(this.createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setCreatedAt(String createdAt) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        try {
            this.createdAt = sdf.parse(createdAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    @NonNull
    public String toString() {
        return buddy + ": " + message;
    }
}

